#!/usr/bin/python3
#

import os
import secrets
import time
import random
import pensive
import discord
from discord.ext import commands

from quotes import facts

intents = discord.Intents.default()
intents.members = True

bot = commands.Bot(intents=intents, command_prefix='!', help_command=None)

os.environ['TZ'] = "EST"
time.tzset()

p = pensive.Pensive()
token = p.get_auth_token(p.client_info, p.provider_url)

greets = [
    "hello",
    "hiya",
    "hey",
    "sup",
    "yo",
    "hai"
]

hello = [
    ["Hello there, ", "!"],
    ["Hi, ", "!"],
    ["How's it going, ", "?"],
    ["What's up, ", "?"],
    ["Hello, ", " my sunshine!"],
    ["What's kicking, little chicken, ", "?"],
    ["My name's Ralph, and I'm a bad guy, ", "."],
    ["Ahoy, ", ", matey!"],
    ["Put that cookie down, ", "!"],
    ["Hiya, ", "!"],
    ["'Ello, ", "gov'nor!"],
    ["Top of the mornin' to ya, ", "!"],
    ["What's crackin', ", "?"],
    ["This call may be recorded for training purposes, ", "."],
    ["How does a lion greet the other animals in the field, ", "? A: Please to eat you."],
    ["I'm Batman, ", "."],
    ["At last, we meet for the first time for the last time, ", "!"],
    ["Ghostbusters, whatya want, ", "?"],
    ["Oh, yoooouhoooo, ", "!"],
    ["What's cookin', good lookin', ", "?"],
    ["I like your face, ", "."],
    ["Aloha, ", "!"],
    ["E aí, tudo bem, ", "?"],
    ["Hola que tal, ", "?"],
    ["Konnichiwa, ", "-san."],
    ["Ciao, ", "!"],
    ["Bonjour, ", "!"],
    ["Guten Tag, ", "!"],
    ["Greetings, carbon-based life-form, ", " is your designation, correct?"],
    ["How YOU doin', ", "?"],
    ["Lookin' good, ", "! How are you?"],
    ["Wagwan, ", "?"],
    ["Anaria shola, ", "."],
    ["Bal'a dash, malanore, ", "."],
    ["The eternal sun guides us, ", "."],
    ["Spit it out, I got smeltin' to do, ", "!"],
    ["May the Black Anvil bless ye, ", "."],
    ["Lord Illidan knows the way, ", "!"],
    ["Archenon poros, ", "."],
    ["Well met, ", "!"],
    ["Interest ya'n a pint, ", "?"],
    ["I am looking forward to our... negotiation, ", "."],
    ["This had better be good, ", "."],
    ["What do you require, ", "?"],
    ["Pleased to meet you, ", "!"],
    ["Need assistance, ", "?"],
    ["Alrighty then, ", "!"],
    ["Time is money, ", "."],
    ["Well? Spit it out, ", "!"],
    ["G.T.L., ", ": Gambling, Tinkering, Laundry!"],
    ["King's honor, ", "."],
    ["Lord Admiral's favor, ", "."],
    ["Tanu'balah, ", "."],
    ["Ash'thoras danil, ", "."],
    ["Ishnu-alah, ", "."],
    ["Elune be with you, ", "."],
    ["Zug-zug, ", "!"],
    ["Lok-tar, ", "!"],
    ["Something need doing, ", "?"],
    ["The winds guide you, ", "."],
    ["Ah, I've been expecting you, ", "."]
]

def check_string(greets, string):
    for g in greets:
        if(g in string.split()):
            return True
    return False

@bot.event
async def on_ready():
    for g in bot.guilds:
        print(f"Joined server: {g.name} ({g.id})")

@bot.command(name="time")
async def _time(ctx):
    t = time.strftime("%T %x")
    await ctx.send(f"Time is now {t}")

@bot.command()
async def status(ctx, *args):
    await ctx.send(f"Okay, gimme a sec...")

    if(len(args) == 0):
        arg = "dalaran"
    else:
        arg = '-'.join(args)

    name, realmid = p.get_realm_id(arg.lower(), token=token)

    if(realmid is False):
        await ctx.send(f"Errr, sorry, I don't know any realm by that name, {ctx.author.mention}...")
        return

    status, has_queue = p.get_realm_status(realmid, "us", token)

    msg = f"{name} is now {status.lower()}."
    if("up" in status.lower()):
        if(has_queue == True):
            msg += " It has a queue, though."
        else:
            msg += " No queue!"

    await ctx.send(msg)

@bot.command(name="help")
async def _help(ctx, arg=None):
    if(arg is not None):
        msg = f"Hey there, {arg.mention}!\n"
    else:
        msg  = f"Hey there, {ctx.author.mention}!\n"
    msg += "Here are some commands you can use with me.\n"
    msg += "--------------------------------------------------------\n"
    msg += "Say hello and I will respond! I can say a lot of things!\n\n"
    msg += "**!help** will bring up this help message\n"
    msg += "**!status** **<name-of-realm>** will report the status of that realm (for now, only works on US realms);\n"
    msg += "             if you just say **!status** I'll assume you're talking about Dalaran!\n"
    msg += "**!time** and I will tell you the current realm time!\n"
    msg += "**!fact** and I will throw a random fact at you!\n"
    msg += "**!mythics <character-name>** and I'll look up your best mythic runs!\n"
    msg += "**!vault <character-name>** and I'll see the item levels you're gonna get in the vault form mythics!\n"
    msg += "--------------------------------------------------------\n"
    msg += "When in doubt, ask <@310819858256363520>!"

    await ctx.send(msg)

@bot.command(name="fact")
async def random_fact(ctx):
    quote = random.choice(facts)
    msg = f"Hey, {ctx.author.mention}, did you know?\n"

    await ctx.send(msg + quote)

@bot.command(name="mythics")
async def best_runs(ctx, arg):
    args = arg.split("-")
    if(len(args) == 1):
        data = p.get_char_mythics(charname=args[0])
    elif(len(args) > 1):
        data = p.get_char_mythics(charname=args[0], realmname=args[1])
    msg = f"Hey, {ctx.author.mention}, here you go:\n"
    msg += p.report_mythics(data)

    for c in ctx.guild.channels:
        if(c.name == "bot-channel"):
            channel = bot.get_channel(c.id)
            await channel.send(msg)
            return


@bot.command(name="vault")
async def vault(ctx, arg):
    args = arg.split("-")
    if(len(args) == 1):
        data = p.get_char_mythics(charname=args[0])
    elif(len(args) > 1):
        data = p.get_char_mythics(charname=args[0], realmname=args[1])
    vault_msg = f"Hey, {ctx.author.mention}, here you go:\n"
    vault_msg += p.vault_from_mythics(data)
    for c in ctx.guild.channels:
        if(c.name == "bot-channel"):
            channel = bot.get_channel(c.id)
            await channel.send(msg)
            return
    # await ctx.send(vault_msg)
    # return

@bot.listen('on_message')
async def respond(message):
    if(message.author == bot.user):
        return

    if(bot.user.id not in message.raw_mentions):
        return

    msg = message.content.lower()   

    if(check_string(greets, msg)):
        sup = random.choice(hello)
        await message.channel.send(f"{sup[0]}{message.author.mention}{sup[1]}")
        print(f"Messaging {message.author} in channel #{message.channel}")
        return
    elif(check_string(["help"], msg)):
        await _help(message.channel, message.author)
        return
    elif(check_string(["fundamental question", "meaning of life"], msg)):
        await message.channel.send(f"The answer to the fundamental question about Life, the Universe and Everything is 42, of course!")
        return
    else:
        quote = random.choice(facts)
        msg = f"Well, {message.author.mention}, you didn't say anything I could respond to so here's a random fact:\n"
        msg += quote
        await message.channel.send(msg)
        return

bot.run(secrets.DISCORD_TOKEN)
