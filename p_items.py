#!/usr/bin/python3
#

import json
import pensive

p = pensive.Pensive()
token = p.get_auth_token(p.client_info, p.provider_url)
auctions = p.get_auction_data(token)

# i = 0
#     for item in auctions:
#         # print(item['item']['id'])
#         if 'unit_price' in item:
#             x = p.find_item_in_list(item['item']['id'], p.item_ids)
#             if(x != -1):
#                 print(i, item['item']['id'], item['unit_price'], x)
#         i += 1


for item in auctions:
    x = p.find_item_in_list(item['item']['id'], p.item_ids)
    if('unit_price' in item and x != -1):
        buyout = item['unit_price']
        if(p.qtd[x] == 0):
            p.bo_min[x] = buyout
            p.bo_max[x] = p.bo_min[x]
        p.avg[x] = int((p.avg[x]*p.qtd[x] + buyout*item['quantity'])/(p.qtd[x] + item['quantity']))
        p.qtd[x] += item['quantity']
        if(buyout < p.bo_min[x]):
            p.bo_min[x] = buyout
        if(buyout > p.bo_max[x]):
            p.bo_max[x] = buyout
        # print(x, p.item_ids[x], p.qtd[x], p.format_price(p.avg[x]), p.format_price(p.bo_min[x]), p.format_price(p.bo_max[x]))


for i in range(p.n_items):
    p.save_item_prices(p.item_ids[i], p.qtd[i], p.avg[i], p.bo_min[i], p.bo_max[i])
    if(p.qtd[i] != 0):
        print(p.qtd[i], p.get_item_name(token, p.item_ids[i]), "/", p.format_price(p.avg[i]), "-", p.format_price(p.bo_min[i]), "-", p.format_price(p.bo_max[i]))
