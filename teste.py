from pensive import Pensive
# import array
import tweepy
import time
import calendar
import datetime as dt

p = Pensive()


# sl_launch = dt.date(2020, 11, 23)

auth = tweepy.OAuthHandler(p.twitter_info['twitter_id'], p.twitter_info['twitter_key'])
auth.set_access_token(p.twitter_info['twitter_access'], p.twitter_info['twitter_secret'])
api = tweepy.API(auth)

# class PensiveListener(tweepy.StreamListener):

#     def on_status(self, status):
#         tweet = api.get_status(status.id, tweet_mode='extended')
#         print(f"{tweet.author.name}: {tweet.full_text}")

#     def on_error(self, status_code):
#         if(status_code == 420):
#             return False

# penListener = PensiveListener()
# stream = tweepy.Stream(auth = api.auth, listener=penListener, tweet_mode='extended')

# # stream.filter(follow=['174307074'])
# stream.filter(track=["Warcraft"])


date = dt.date.today()
year = date.strftime("%Y")

i = 0
j = 0
for item in tweepy.Cursor(api.user_timeline, id=174307074, tweet_mode='extended').items()   :
    text = item._json['full_text']
    i += 1
    if(text.startswith("MAINTENANCE")):
        j += 1
        a = text.find("#Warcraft")
        b = text.find("(PST)")
        if(b == -1):
            b = text.find("(PDT)")
        tt = text[a:b]
        a = text.find(":")
        date = text[a-5:a]
        a = tt.find(":")
        ttt = tt[a+1:].split("-")
        start = time.strptime(f"{year} {date} {ttt[0].strip()}", "%Y %m/%d %I:%M %p")
        end = time.strptime(f"{year} {date} {ttt[1].strip()}", "%Y %m/%d %I:%M %p")
        f_start = time.strftime("%x %X", start), calendar.timegm(start)
        f_end = time.strftime("%x %X", end), calendar.timegm(end)
        print(i)
        print(f"start: {f_start[0]} - end: {f_end[0]}")
        print(f"start: {f_start[1]} - end: {f_end[1]}")

    if(j == 3):
        exit()

        

print(i)

# classes = p.get_classes()
# races = p.get_races()

# print("Fetching Horde roster...")
# horde = p.get_guild_roster(guildname='pensive')
# print("Fetching Alliance roster...")
# alliance = p.get_guild_roster(guildname='pénsive')

# roster = horde+alliance

# n = len(roster)

# a, b = p.run_roster_statistics(roster, classes, races)

# # print(a)
# print("\nClass Distribution\n============================")
# for i in a:
#     per = 100*int(a[i])/n
#     print(f"{classes[i]}: {a[i]} ({per:.1f}%)")

# # print(b)
# print("\nRace Distribution\n======================")
# for i in b:
#     per = 100*int(b[i])/n
#     print(f"{races[i]}: {b[i]} ({per:.1f}%)")



# data = p.get_char_mythics(charname='svergun')

# if(not data):
#     print(f"No mythic runs found for this character.")
#     exit()

# p.report_mythics(data)

# keys = data['keys']

# print(keys)

# print(keys[0], p.mythics_vault_rewards[str(keys[0])])
# if(len(keys) >= 4):
#     print(keys[3], p.mythics_vault_rewards[str(keys[3])])
#     if(len(keys) >= 10):
#         print(keys[9], p.mythics_vault_rewards[str(keys[9])])
