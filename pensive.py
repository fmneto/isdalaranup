#!/usr/bin/python3
#

import os
import requests
import json
import secrets
import tweepy
import time
import psycopg2
import discord

from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session

class Pensive:

    def __init__(self):

        self.client_info = {
            'client_id': secrets.CLIENT_ID,
            'client_secret': secrets.CLIENT_SECRET
        }

        self.twitter_info = {
            'twitter_id': secrets.TWITTER_ID,
            'twitter_key': secrets.TWITTER_KEY,
            'twitter_access': secrets.TWITER_A_TOKEN,
            'twitter_secret': secrets.TWITTER_A_SECRET
        }

        self.provider_url = "https://us.battle.net/oauth/token"

        self.key_ilvl = {
            '2':	'200',
            '3':	'203',
            '4':	'207',
            '5':	'210',
            '6':	'210',
            '7':	'213',
            '8':	'216',
            '9':	'216',
            '10':	'220',
            '11':	'220',
            '12':	'223',
            '13':	'223',
            '14':	'226',
            '15':	'226'
        }

        # Connect to our database
        try:
            self.c = psycopg2.connect(
            host = "datalore",
            database = "isdalaranup",
            user = "idu",
            password = "a6s%t)0")
            self.cur = self.c.cursor()
        except Exception as e:
            print(e)
            exit()
        

        # Let's try to fetch our current authorization token - always the latest one
        self.cur.execute("select * from auth_token where auth_token.valid_until > extract(epoch from now()) order by tstamp desc limit 1")

        row = self.cur.fetchone()

        if row is None:
            self.token = None
        else:
            self.token = row[3]

        self.item_ids = []
        self.qtd = []
        self.avg = []
        self.bo_min = []
        self.bo_max = []
        with open("itemlist.dat", 'r') as f:
            line = f.readline()
            while(line):
                itemid = int(line)
                self.item_ids.append(itemid)
                self.qtd.append(0)
                self.avg.append(0)
                self.bo_min.append(0)
                self.bo_max.append(0)
                line = f.readline()

        self.item_ids.sort()

        self.n_items = len(self.item_ids)


    def get_auth_token(self, client_info, url):

        client = BackendApplicationClient(client_id=client_info['client_id'])
        oauth = OAuth2Session(client=client)
        token = oauth.fetch_token(token_url=url, client_id=client_info['client_id'], client_secret=client_info['client_secret'])

        values = "(DEFAULT, " + str(time.time()) + ", " + str(token['expires_at']) + ", '" + str(token['access_token']) + "')"
        query = "insert into auth_token values " + values

        self.cur.execute(query)

        return token['access_token']

    def check_auth_token(self, token=None):

        if(token == None):  # Token is brand new; ideally, this should not happen except when the bot is initialized
            ret = self.get_auth_token(self.client_info, self.provider_url)
        else:
            url = "https://us.battle.net/oauth/check_token"
            params = {
                'region': 'us',
                'token': token
            }
            resp = requests.post(url, data=params)

            if(resp.status_code == 200):  # Token is still valid
                ret = token
            else:  # Token has expired
                ret = self.get_auth_token(self.client_info, self.provider_url)

        return ret

    def get_realm_status(self, realmid, region, token=None):
        access_token = self.check_auth_token(token)
        request = f"https://us.api.blizzard.com/data/wow/connected-realm/{realmid}?namespace=dynamic-us&locale=en_US"

        resp = requests.get(request, headers={'Authorization': 'Bearer ' + access_token})

        cont = resp.json()
        return cont['status']['name'], cont['has_queue']


    def get_realm_id(self, slug, locale="en_US", token=None):
        access_token = self.check_auth_token(token)
        request = f"https://us.api.blizzard.com/data/wow/realm/{slug}?namespace=dynamic-us"

        resp = requests.get(request, headers={'Authorization': 'Bearer ' + access_token})
        if(not resp.ok):
            return False, False
        
        cont = resp.json()
        url = cont['connected_realm']['href']

        return cont['name'][locale], url.split("/")[6].split("?")[0]

    def get_dalaran_status(self, token=None):
        access_token = self.check_auth_token(token)
        request = "https://us.api.blizzard.com/data/wow/connected-realm/3683?namespace=dynamic-us&locale=en_US"
        
        resp = requests.get(request, headers={'Authorization': 'Bearer ' + access_token})

        cont = resp.json()
        return cont['status']['name']

    def save_dalaran_status(self, realmid, status):
        query = "insert into realm_status values (DEFAULT, " + str(realmid) + ", " + str(int(time.time())) + ", "
        query += "1" if(status == "Up") else "0"
        query += ")"
        print(query)
        self.cur.execute(query)
        self.c.commit()

    def post_twitter_update(self, text):
        auth = tweepy.OAuthHandler(self.twitter_info['twitter_id'], self.twitter_info['twitter_key'])
        auth.set_access_token(self.twitter_info['twitter_access'], self.twitter_info['twitter_secret'])
        twitter_api = tweepy.API(auth)

        try:
            twitter_api.update_status(text)
        except tweepy.error.TweepError as e:
            print(e.reason)

        print("Sending", text)

    def get_saved_status(self, realmid):
        self.cur.execute(f"select * from realm_status where realm_id={realmid} order by tstamp desc limit 1")
        row = self.cur.fetchone()

        return "Up" if row[3] == 1 else "Down"

    def get_random_fact(self, mention):
        self.cur.execute(f"select * from ppl_facts offset random() * (select count(*) from ppl_facts) limit 1")
        row = self.cur.fetchone()

        return row[1].replace("__mention__", mention)


    def get_auction_data(self, token):
        access_token = self.check_auth_token(token)
        request = "https://us.api.blizzard.com/data/wow/connected-realm/3683/auctions?namespace=dynamic-us"

        resp = requests.get(request, headers={'Authorization': 'Bearer ' + access_token})

        item_ids = resp.json()

        return item_ids['auctions']

    def get_item_prices(self, token, itemid, auction_data = None):
        if(auction_data is None):
            return False

        qtd = 0
        buyout = 0

        for item in auction_data:
            if(item['item']['id'] == itemid):
                if(qtd == 0):
                    min = item['buyout']
                    max = min
                qtd += item['quantity']
                buyout += item['buyout']
                if(item['buyout'] < min):
                    min = item['buyout']
                if(item['buyout'] > max):
                    max = item['buyout']

        return self.get_item_name(token, itemid), qtd, int(buyout/qtd), min, max

    def save_item_prices(self, itemid, qtd, avg, min, max):
        query = "insert into item_prices values (DEFAULT, " + str(int(time.time())) + ", " + str(itemid) + ", "
        query += str(qtd) + ", " + str(avg) + ", " + str(min) + ", " + str(max) + ");"

        try:
            self.cur.execute(query)
            self.c.commit()
            return True
        except:
            return False

    def drop_item_from_database(self, itemid):
        query = f"delete from item_prices where itemid={itemid}"
        self.cur.execute(query)
        self.c.commit()

    def search_for_item(self, itemid, latest = False):

        if(itemid == None):
            return False
        
        if(latest is False):
            res = []
            query = f"select * from item_prices where itemid='{itemid}'"
            self.cur.execute(query)
            row = self.cur.fetchone()
            while row is not None:
                a = []
                a.append(time.gmtime(row[1]))
                a.append(row[3])
                a.append(row[4])
                res.append(a)
                row = self.cur.fetchone()
            return res
        else:
            query = f"select * from item_prices where itemid='{itemid}' order by tstamp desc limit 1"
            self.cur.execute(query)
            row = self.cur.fetchone()
            if(row is not None):
                a = []
                a.append(time.gmtime[row[1]])
                a.append(row[3])
                a.append(row[4])
                res.append(a)
                return res

        return False

    def get_item_history(self, itemid, field="min"):

        if(itemid == None):
            raise ValueError("itemid is required")

        if("field" == "max"):
            select = "max"
        elif("field" == "avg"):
            select ="avg"
        else:
            select = "min"

        query = f"select tstamp,{select} from item_prices where itemid='{itemid}' order by tstamp"
        ret = []
        self.cur.execute(query)
        row = self.cur.fetchone()
        while row is not None:
            x = []
            x.append(row[0])
            x.append(row[1])
            ret.append(x)
            row = self.cur.fetchone()

        return ret

    def get_item_name(self, token, itemid, locale='en_US'):
        access_token = self.check_auth_token(token)
        request = "https://us.api.blizzard.com/data/wow/item/" + str(itemid) + "?namespace=static-us&locale=" + locale

        resp = requests.get(request, headers={'Authorization': 'Bearer ' + access_token})
        
        item = resp.json()
        return item['name']

    def format_price(self, price):
        s = ""
        s += str(price)[:-4] + "g " + str(price)[-4:-2] + "s " + str(price)[-2:] + "c"

        return s

    def find_item_in_list(self, itemid, itemlist):
        try:
            x = itemlist.index(itemid)
        except ValueError:
            x = -1
        return x

    # def send_status_update_to_discord(self, msg, channel="general-wow"):

    #     intents = discord.Intents.default()
    #     intents.members = True

    #     client = discord.Client(intents=intents)
        
    #     @client.event
    #     async def on_ready():
    #         g = client.guilds[0]
    #         print(f"Server: {g.name} ({g.id})")

    #         t = time.strftime("%x %X")
    #         print(msg)

    #         for c in g.channels:
    #             if(c.name == channel):
    #                 c = client.get_channel(c.id)
    #                 await c.send(msg)
    #                 await client.close()

    #     client.run(secrets.DISCORD_TOKEN)

    def get_char_mythics(self, charname, realmname='dalaran', token='None'):

        if(charname):
            access_token = self.check_auth_token(token)

            char = str(charname).lower()
            realm = str(realmname).lower()

            request = f"https://us.api.blizzard.com/profile/wow/character/{realm}/{char}/mythic-keystone-profile?namespace=profile-us&locale=en_US"
            resp = requests.get(request, headers={'Authorization': 'Bearer ' + access_token})

            if(resp.status_code >= 400):
                return None

            content = resp.json()

            try:
                runs = content['current_period']['best_runs']
            except KeyError:
                return None

            keys = []
            mythics = []
            for item in runs:
                x = {}
                x['name'] = item['dungeon']['name']
                x['level'] = item['keystone_level']
                x['finished'] = item['is_completed_within_time']
                mythics.append(x)
                keys.append(x['level'])

            keys.sort(reverse=True)

            data = {}
            data['name'] = content['character']['name']
            data['realm'] = content['character']['realm']['name']
            data['mythics'] = mythics
            data['keys'] = keys

            return data

        return None

    def report_mythics(self, data):

        if(data == None):
            print("No mythic keystone this week yet!")
            return

        name = data['name']
        realm = data['realm']
        mythics = data['mythics']
        keys = data['keys']

        msg = ""

        msg += f"**{len(mythics)} keys done by character {name}-{realm}.**\n"

        for item in mythics:
            str = f"{item['name']}, level {item['level']} "
            str += "(not " if not item['finished'] else "("
            str += "timed)"
            msg += str + "\n"

        return msg

    def mythics_vault_rewards(self, key_level):
        if(key_level >= 15):
            return self.key_ilvl['15']
        else:
            return self.key_ilvl[str(key_level)]

    def vault_from_mythics(self, data):
        if(not data):
            msg = "No mythic runs found for that character."
            return msg

        name = data['name']
        realm = data['realm']
        keys = data['keys']

        msg = f"**Vault rewards from mythic runs for character {name}-{realm}:**\n"

        msg += f"1: Item level {self.mythics_vault_rewards(keys[0])} (keystone level {keys[0]})\n"
        print(keys[0], self.mythics_vault_rewards(keys[0]))
        if(len(keys) >= 4):
            msg += f"2: Item level {self.mythics_vault_rewards(keys[3])} (keystone level {keys[3]})\n"
            print(keys[3], self.mythics_vault_rewards(keys[3]))
            if(len(keys) >= 10):
                msg += f"3: Item level {self.mythics_vault_rewards(keys[9])} (keystone level {keys[9]})\n"
                print(keys[9], self.mythics_vault_rewards(keys[9]))

        return msg


    def get_guild_roster(self, guildname, realmname='dalaran', token=None):

        if(guildname):
            access_token = self.check_auth_token(token)

            guild = str(guildname).lower()
            realm = str(realmname).lower()

            request = f"https://us.api.blizzard.com/data/wow/guild/{realm}/{guild}/roster?namespace=profile-us&locale=en_US"
            resp = requests.get(request, headers={'Authorization': 'Bearer ' + access_token})

            if(resp.status_code >= 400):
                return None

            content = resp.json()

            try:
                guild_info = content['guild']
            except KeyError:
                return None

            guild_name = guild_info['name']
            guild_realm = guild_info['realm']['name']

            print(f"Guild {guild_name} from server {guild_realm}")

            members = content['members']

            guild_members = []

            i = 1
            for item in members:
                a = []
                a.append(item['character']['name'])
                a.append(str(item['character']['playable_class']['id']))
                a.append(str(item['character']['playable_race']['id']))
                i += 1
                guild_members.append(a)

            return guild_members

        return None

    @staticmethod
    def run_roster_statistics(roster, classes, races):

        class_stat = classes.copy()
        for i in class_stat:
            class_stat[i] = 0
        races_stat = races.copy()
        for i in races_stat:
            races_stat[i] = 0

        for member in roster:
            class_stat[member[1]] = class_stat[member[1]]+1
            races_stat[member[2]] = races_stat[member[2]]+1

        return class_stat, races_stat

    def get_classes(self, token=None):

        access_token = self.check_auth_token(token)

        request = f"https://us.api.blizzard.com/data/wow/playable-class/index?namespace=static-us&locale=en_US"
        resp = requests.get(request, headers={'Authorization': 'Bearer ' + access_token})

        if(resp.status_code >= 400):
            return None

        content = resp.json()
        classes_info = content['classes']
        classes = {}

        for item in classes_info:
            id = str(item['id'])
            classes[id] = item['name']

        return classes

    def get_races(self, token=None):

        access_token = self.check_auth_token(token)

        request = f"https://us.api.blizzard.com/data/wow/playable-race/index?namespace=static-us&locale=en_US"
        resp = requests.get(request, headers={'Authorization': 'Bearer ' + access_token})

        if(resp.status_code >= 400):
            return None

        content = resp.json()

        races_info = content['races']
        races = {}
        for item in races_info:
            id = str(item['id'])
            races[id] = item['name']

        races.pop('24')
        races['25'] = "Alliance Pandaren"
        races['26'] = "Horde Pandaren"

        return races
