#!/usr/bin/python3
#

from apscheduler.schedulers.blocking import BlockingScheduler

import pensive as p
import time
import json

def check_for_dalaran():
    bot = p.Pensive()
    old_status = bot.get_saved_status()
    status = bot.get_dalaran_status(bot.token)
    msg = "Dalaran is now " + status + "!"
    if(status != old_status):
        old_status = status
        print(time.strftime("%d/%m/%Y %H:%M:%S"), ":", msg)
        bot.save_dalaran_status(status)
        bot.post_twitter_update(msg)
        bot.send_status_update_to_discord(msg)
    else:
        print("Dalaran is still " + old_status + ".")

if __name__ == "__main__":
    sched = BlockingScheduler()

    sched.add_executor('processpool')
    sched.add_job(check_for_dalaran, 'interval', seconds=15)

    try:
        sched.start()
    except (KeyboardInterrupt,SystemExit):
        pass
