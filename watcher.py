#!/usr/bin/python3
#

import os
import secrets
import time
import asyncio
import pensive
import random
import discord
from discord.ext import commands
import tweepy
from apscheduler.schedulers.asyncio import AsyncIOScheduler as AIOSched
from quotes import greets, hello, facts

p = pensive.Pensive()

auth = tweepy.OAuthHandler(p.twitter_info['twitter_id'], p.twitter_info['twitter_key'])
auth.set_access_token(p.twitter_info['twitter_access'], p.twitter_info['twitter_secret'])
api = tweepy.API(auth)

os.environ['TZ'] = "EST"
time.tzset()


class PensiveListener(tweepy.StreamListener):

    def __init__(self, discord, loop, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.discord = discord
        self.loop = loop
        print("Listener initialized")

    def on_status(self, status):
        tweet = api.get_status(status.id, tweet_mode='extended')
        msg = f"{tweet.author.name}: {tweet.full_text}"
        text = tweet.full_text
        if(text.startswith("MAINTENANCE")):
            a = text.find("#Warcraft")
            b = text.find("(PST)")
            if(b == -1):
                b = text.find("(PDT)")
            tt = text[a:b+5].replace("&amp;", "&")
            print(tt)

        self.send_message(msg, guild_id=807603173182668810)

    def send_message(self, msg, guild_id=None):
        future = asyncio.run_coroutine_threadsafe(self.discord(msg, guild_id=guild_id), self.loop)
        future.result()

    def on_error(self, status_code):
        if(status_code == 420):
            return False

class PensiveDiscord(discord.ext.commands.Bot):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        print("Bot initialized")

        self.sched = AIOSched({'apscheduler.job_defaults.max_instances': '5'})
        self.sched.add_job(self.check_for_dalaran, 'interval', seconds=10)
        self.sched.add_job(self.update_auction_data, 'interval', hours=2)
        self.sched.start()
        print(f"Scheduler initialized")

    async def check_for_dalaran(self):
        old_status = p.get_saved_status(3683)
        status = p.get_dalaran_status(p.token)
        msg = "Dalaran is now " + status + "!"
        if(status != old_status):
            old_status = status
            tstamp = time.strftime("%d/%m/%Y %H:%M:%S")
            print(f"{tstamp}: {msg}")
            p.save_dalaran_status(3683, status)
            p.post_twitter_update(msg)
            await self.send_msg(msg, channel="general-wow")
        else:
            print("Dalaran is still " + old_status + ".")

    async def update_auction_data(self):
        auctions = p.get_auction_data(p.token)
        for item in auctions:
            x = p.find_item_in_list(item['item']['id'], p.item_ids)
            if('unit_price' in item and x != -1):
                buyout = item['unit_price']
            if(p.qtd[x] == 0):
                p.bo_min[x] = buyout
                p.bo_max[x] = p.bo_min[x]
                p.avg[x] = int((p.avg[x]*p.qtd[x] + buyout*item['quantity'])/(p.qtd[x] + item['quantity']))
                p.qtd[x] += item['quantity']
            if(buyout < p.bo_min[x]):
                p.bo_min[x] = buyout
            if(buyout > p.bo_max[x]):
                p.bo_max[x] = buyout


        for i in range(p.n_items):
            p.save_item_prices(p.item_ids[i], p.qtd[i], p.avg[i], p.bo_min[i], p.bo_max[i])
            # if(p.qtd[i] != 0):
            #     print(p.qtd[i], p.get_item_name(token, p.item_ids[i]), "/", p.format_price(p.avg[i]), "-", p.format_price(p.bo_min[i]), "-", p.format_price(p.bo_max[i]))
        print("Auction data update complete")

    async def on_ready(self):
        for g in self.guilds:
            print(f"Joined server: {g.name} ({g.id})")

        PensiveStream = tweepy.Stream(auth = api.auth, listener=PensiveListener(discord=self.send_msg, loop=asyncio.get_event_loop()))
        PensiveStream.filter(follow=['174307074'], is_async=True)

    @staticmethod
    async def send_msg(msg, guild_id=None, channel="bot-channel"):
        if(guild_id is not None):
            g = bot.get_guild(guild_id)
            for c in g.channels:
                if(c.name == channel):
                    print(f"Sending to {g.name}:{c.name}")
                    ctx = bot.get_channel(c.id)
                    await ctx.send(msg)
        else:
            for g in bot.guilds:
                for c in g.channels:
                    if(c.name == channel):
                        print(f"Sending to {g.name}:{c.name}")
                        ctx = bot.get_channel(c.id)
                        await ctx.send(msg)

    @staticmethod
    def check_string(greets, string):
        for g in greets:
            if(g in string.split()):
                return True
        return False

intents = discord.Intents.default()
intents.members = True
bot = PensiveDiscord(command_prefix="!", intents = intents, help_command=None)

@bot.command(name="time")
async def _time(ctx):
    t = time.strftime("%T %x")
    await ctx.send(f"Time is now {t}")

@bot.command()
async def status(ctx, *args):
    await ctx.send(f"Okay, gimme a sec...")

    if(len(args) == 0):
        arg = "dalaran"
    else:
        arg = '-'.join(args)

    name, realmid = p.get_realm_id(arg.lower(), token=p.token)

    if(realmid is False):
        await ctx.send(f"Errr, sorry, I don't know any realm by that name, {ctx.author.mention}...")
        return

    status, has_queue = p.get_realm_status(realmid, "us", p.token)

    msg = f"{name} is now {status.lower()}."
    if("up" in status.lower()):
        if(has_queue == True):
            msg += " It has a queue, though."
        else:
            msg += " No queue!"

    await ctx.send(msg)

@bot.command(name="help")
async def _help(ctx, arg=None):
    if(arg is not None):
        msg = f"Hey there, {arg.mention}!\n"
    else:
        msg  = f"Hey there, {ctx.author.mention}!\n"
    msg += "Here are some commands you can use with me.\n"
    msg += "--------------------------------------------------------\n"
    msg += "Say hello and I will respond! I can say a lot of things!\n\n"
    msg += "**!help** will bring up this help message\n"
    msg += "**!status** **<name-of-realm>** will report the status of that realm (for now, only works on US realms);\n"
    msg += "             if you just say **!status** I'll assume you're talking about Dalaran!\n"
    msg += "**!time** and I will tell you the current realm time!\n"
    msg += "**!fact** and I will throw a random fact at you!\n"
    msg += "**!fact <mention> if you mention people I'll throw a fact about them instead!\n"
    msg += "**!mythics <character-name>** and I'll look up your best mythic runs!\n"
    msg += "**!vault <character-name>** and I'll see the item levels you're gonna get in the vault form mythics!\n"
    msg += "--------------------------------------------------------\n"
    msg += "When in doubt, ask <@310819858256363520>!"

    await ctx.send(msg)

@bot.command(name="fact")
async def random_fact(ctx):
    if(len(ctx.message.mentions) == 0):
        quote = random.choice(facts)
        msg = f"Hey, {ctx.author.mention}, did you know?\n"
        await ctx.send(msg + quote)
    else:
        i = 0
        for x in ctx.message.mentions:
            if(x.name != bot.user.name and i < 3):
                quote = p.get_random_fact(x.mention)
                await ctx.send(quote)

@bot.command(name="mythics")
async def best_runs(ctx, arg):
    args = arg.split("-")
    if(len(args) == 1):
        data = p.get_char_mythics(charname=args[0])
    elif(len(args) > 1):
        data = p.get_char_mythics(charname=args[0], realmname=args[1])
    msg = f"Hey, {ctx.author.mention}, here you go:\n"
    msg += p.report_mythics(data)

    for c in ctx.guild.channels:
        if(c.name == "bot-channel"):
            channel = bot.get_channel(c.id)
            await channel.send(msg)
            return

@bot.command(name="vault")
async def vault(ctx, arg):
    args = arg.split("-")
    if(len(args) == 1):
        data = p.get_char_mythics(charname=args[0])
    elif(len(args) > 1):
        data = p.get_char_mythics(charname=args[0], realmname=args[1])
    vault_msg = f"Hey, {ctx.author.mention}, here you go:\n"
    vault_msg += p.vault_from_mythics(data)
    for c in ctx.guild.channels:
        if(c.name == "bot-channel"):
            channel = bot.get_channel(c.id)
            await channel.send(msg)
            return
    # await ctx.send(vault_msg)
    # return

@bot.listen('on_message')
async def respond(message):
    if(message.author == bot.user):
        return

    if(bot.user.id not in message.raw_mentions):
        return

    msg = message.content.lower()   

    if(bot.check_string(greets, msg)):
        sup = random.choice(hello)
        await message.channel.send(f"{sup[0]}{message.author.mention}{sup[1]}")
        print(f"Messaging {message.author} in channel #{message.channel}")
        return
    elif(bot.check_string(["help"], msg)):
        await _help(message.channel, message.author)
        return
    elif(bot.check_string(["fundamental question", "meaning of life"], msg)):
        await message.channel.send(f"The answer to the fundamental question about Life, the Universe and Everything is 42, of course!")
        return
    else:
        quote = random.choice(facts)
        msg = f"Sorry, {message.author.mention}, I didn't quite catch what you meant. To compensate, here's a random fact:\n"
        msg += quote
        await message.channel.send(msg)
        return

try:
    bot.run(secrets.DISCORD_TOKEN)
except KeyboardInterrupt:
    exit()
except Exception as e:
    print(e.msg)
    exit()
