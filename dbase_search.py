#!/usr/bin/python3
#

import json
import pensive
import time

p = pensive.Pensive()
token = p.get_auth_token(p.client_info, p.provider_url)
# auctions = p.get_auction_data(token)

# for i in p.item_ids:
#     name = p.get_item_name(token, i)
#     item_string, n = p.search_for_item(i, latest=True)
#     print(f"Item: {name} ({i})")
#     print(item_string)
#     print("==================================================")

for i in p.item_ids:
    x = p.get_item_history(i)
    print(f"{p.get_item_name(token, i)} ({i}):")
    for y in x:
        if(y[1] != 0):
            t = time.strftime("%d/%m, %H:%M", time.gmtime(y[0]))
            print(f"{t}: {p.format_price(y[1])}")
    print("---------------------------------")